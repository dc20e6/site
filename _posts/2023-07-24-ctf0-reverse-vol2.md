---
layout: post
title: Разбор заданий по реверсу с DC78422 CTF0 (часть 2) 
date: 2023-07-24 00:00:05
categories: writeup
short_description: Разбор заданий по реверсу с DC78422 CTF0 (Deep.exe)
image_preview: https://i.gyazo.com/07c9f1ab88f511a3941234b133504a47.png
---

> Данное описание подготовлено и публикуется со слов участника CTF, занявшего [почетное второе место](https://t.me/dc20e6/46726) – Сементинова Сергея ( [@cema_rus](https://t.me/cema_rus) ).
> Мы разбили прохождение реверста на несколько статей потому что они достаточно обширны и подробны, это уже вторая часть, [первая часть разбором с заданиями Task1.exe, Task2.exe, Task3.exe, taskSyzran.exe](https://dc78422.ru/writeup/2023/07/24/ctf0-reverse.html) уже доступна, как и [третья часть разбора с финальным заланием Final.exe](https://dc78422.ru/writeup/2023/07/24/ctf0-reverse-vol3.html).
> Дальше текст передается as-is.

## Deep.exe

Предпоследнее задание. Уже больше похожее на настоящий реверс, со всем плюшками в виде: разобрать код, понять алгоритм и сделать генератор ключей. Ну что) В добрый путь…

Сначала планировалось выполнять это задание с применением все тех же IDR и IDA, но в IDA при декомпиляции вылазила ошибка и, честно сказать, было лень с ней разбираться - было решено использовать “новый для себя” инструмент - Ghidra (https://github.com/NationalSecurityAgency/ghidra/releases).

Качаем, запускаем, создаем проект, добавляем приложение:

![](https://i.gyazo.com/7d440fd47b1dff3520d57f4f9ce84120.png)

По двойному щелчку на приложении запускается окно CodeBrowser и, при  первом запуске, предлагается провести анализ кода, соглашаемся на всё, жмём на кнопку Analyze и дожидаемся окончания анализа.

![](https://i.gyazo.com/9f0b67a0f9bffd0bdfbedfd9c0ffbffb.png)

Теперь имеем код и давайте его разбирать, попутно переименовывая различные переменные и функции для удобного понимания:

![](https://i.gyazo.com/6665d0d0ee5b5b7d46f0d9d31c90a5ea.png)

Как обычно, начинаем с условия, когда флаг считается правильным. Переменная `DAT_0040a800` должна, после всех вычислений, иметь значение равное 100. В самой функции не видно объявления этой переменной, только начальная инициализация. Скорее всего, эта переменная глобальная (в чём мы убедимся позже). Переименовываем и идём дальше.

При дальнейшем изучении понимаем, что функция FUN_00403eb4 возвращает длину строки, а переменная `DAT_0040a798` - это введенный нами влаг. На и тело условия, при помощи логики и фантазии, приводим примерно к такому виду:

![](https://i.gyazo.com/cc9ac0896c4441301db8cc5ed4586e9c.png)

На этом этапе, уже у многих будет общее понимание алгоритма проверки. А за деталями пойдём по очереди в `fInputCharLeft`, `fInputCharRight`, `fInputCharUp` и `fInputCharDown`.

![](https://i.gyazo.com/6be1046f98a498348b7c279a26643c4d.png)

Все 4 функции выглядят примерно одинаково, они отличается битами, наличие которых проверяется в условии и значением, которое суммируется с нашим счетчиком.

Непонятным остаётся только один момент: Что это за условие такое в функциях?! Какое отношение оно имеет к введенному нами флагу?! Ерунда какая-то, которая заставила меня изрядно поломать голову… И, самые внимательные, уже обратили внимание на какую-то странную `+ 3`  в условии.

Продолжаем смотреть другие функции, в надежде что придёт озарение и… Натыкаемся на это:

![](https://i.gyazo.com/7bf4eb0b97a25c0d3b7e65565b83b582.png)

Некая функция с инициализацией некоего массива и, при детальном рассмотрении, видим, что наш массив сидит по адресу `0x0040a79c`, а указатель на введенную нами строку по адресу `0x0040a798`:

![](https://i.gyazo.com/e91ec0ba35a1425985696c976b7edec7.png)

Немного поразмыслив, понимаем, что конструкция вида:

```
*(byte *)((int)&sInputFlag + iCheckingCounter + 3)
```

означает не обращение к нашей строке, а обращение к этому массиву, т.к. `iCheckCounter` не должен быть меньше 1, а общая сумма  `(0x0040a798 + 1 + 3 == 0x0040a79c)` равна адресу нашего массива. В общем… Декомпилятор немного неправильно распознал конструкцию, но оттого стало только интереснее)

Что получаем в итоге? Алгоритм проверки флага:

При запуске `iCheckCounter = 1`;

1. Проверяем все входные условия, в виде фигурных скобок, длины и прочего;
2. Если символ из набора “направлений движения”, то проверяем содержимое элемента массива `someArray[iCheckCounter - 1]` (т.к. у нас индексация начинается с 0) и если установлен бит, соответствующий направлению, идём дальше.
3. Так двигаемся до тех пор, пока `iCheckCounter` не достигнет значения 100;

Алгоритм получили, давайте “накидаем” генератор флагов:

![](https://i.gyazo.com/431be9e7798182115ba8f99181f322b3.png)

Ограничение на длину флага установил в 20 символов, мне показалось, что этого будет достаточно. Запускаем, смотрим результат:

![](https://i.gyazo.com/0f544749c23a47654c1ae7fea1866c7d.jpg)

Выбираем самый короткий, проверяем в программе и на сайте - Correct!

---

Файлы заданий

- [https://dc78422.ru/files/ctf-tasks/0/Deep.exe](https://dc78422.ru/files/ctf-tasks/0/Deep.exe)

