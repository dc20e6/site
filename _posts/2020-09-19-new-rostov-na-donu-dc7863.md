---
layout: post
title: Новое сообщество DC в Ростове-на-Дону DC7863
date: 2020-09-19 00:00:00
categories: news
short_description: В Ростове-на-Дону формируется новое сообщество DC
image_preview: https://avatars1.githubusercontent.com/u/27823111?v=3&s=30
---

Привет,

У в Ростове-на-Дону образовалось новое сообщество DС, а точнее *DC7863*. Ура!

> __cha1ned, [Sep 10, 2020 at 7:05:31 PM]:
> Ну
> я запулил обращение
> во всех чатах ростова
> по поводу создания DC-шки

Если у вас есть знакомые из Ростова, поделитесь этой благой вестью, а подробности можно узнать у [@__cha1ned](https://t.me/cha1ned_l1fe)

P.S. Мы вроде собрали всех сообщества и никого не забыли на странице [https://dc20e6.ru/friends-and-communities/](https://dc20e6.ru/friends-and-communities/)
