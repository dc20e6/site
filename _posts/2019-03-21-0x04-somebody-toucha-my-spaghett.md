---
layout: post
title: SOMEBODY TOUCHA MY SPAGHET на 0x04
date: 2019-03-21 00:00:00
categories: announcement
short_description: Биологические следы человека, как их снять/не оставить.
image_preview: https://avatars1.githubusercontent.com/u/27823111?v=3&s=30
---

# О чем поговорим на встрече 0x04 в докладе "0x01: Кто съел мой обед. Биологические следы человека."

<iframe width="100%" src="https://www.youtube.com/embed/cE1FrqheQNI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Мы все знаем что оставляем кучу различных следов, по которым нас можно определить. Это отпечатки пальцев, кровь, волосы итд. Они вездеееее. Как раз и поговорим о том как определить их, найти и как поступать, чтобы их не оставлять.

Чуть-чуть подробнее можно почитать по ссылке [https://dc20e6.ru/meetup/0x04/#report0x01](https://dc20e6.ru/meetup/0x04/#report0x01). Stay tuned :)