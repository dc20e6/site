---
layout: post
title: Календарь мероприятий DC20e6
date: 2019-10-15 10:00:00
categories: news
short_description: Календарь мероприятий сообщества DC20e6 для удобногого оповещения о местах, времени и прочих штуках
image_preview: https://avatars1.githubusercontent.com/u/27823111?v=3&s=30
---

Крч, мы сделали календарь чтобы через него оповещать о датах, чтобы всем было удобнее собраться и узнать о том, что будет, когда и где.

**[Добавляйте себе календарь и будьте в курсе событий](https://calendar.google.com/calendar?cid=ZXQwbGdyaGVlZHFwcjgwb2I4YWphZTdvOW9AZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ)**