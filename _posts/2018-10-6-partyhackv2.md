---
layout: post
title: PartyHack V2 (Innopolis)
date: 2018-10-06 00:00:00
categories: report
short_description: DC20e6 & PartyHack v2 (Innopolis)
image_preview: https://avatars1.githubusercontent.com/u/27823111?v=3&s=30
---

Привет,

Мы посетили [PartyHack v2](https://partyhack.ru) в Иннополисе и выступили с докладами.
Если вкратце, то было прикольно и весело. Больше напишем попозже :)

### Атака на компанию по-взрослому
Спикер: Ананьев Роман

-  Что такое целевая атака и почему она "ну такое себе"  или если вас хотят поломать, то, да, скорее всего, поломают :)
-  Физическое проникновение на территорию + взрослые игрушки (if_you_know_what_i_mean.jpg);
-  Ваши сайты, сервисы и по говорят все за вас и часто даже больше, чем надо;
-  Что делать и как защищаться (checklist?);
-  Пересмотрите свою инфраструктуру.

[Смотреть доклад {YouTube}](https://www.youtube.com/embed/RpTS9APJHIc)
[Ссылка на презентацию {GoogleDrive}](https://docs.google.com/presentation/d/1IOmF20LyUTl-W712t04QdOwbQmXiiGUA4h7o63Li71o/edit?usp=sharing)

### Инфраструктура: Основы безопасности web проектов
Спикер: Алексей Егорычев

-   Как начать работать над безопасностью web приложения;
-   Проект растет и настало время рассказать ему об опасностях мира;
-   Приложение и конфигурация web сервера. кейсы как не надо делать;
-   Какой вариант деплоя выбрать, кейсы;
-   Расширения для web серверов(modsecurity, naxsi);
-   Анализ поведения пользователей.

[Ссылка на презентацию {GoogleDrive}](https://docs.google.com/presentation/d/1kUvlQPVFXVIHPbeWzNBqMApIALCZjcHU-G8bqARsk04/)
