---
layout: post
title: Встрече 0x08 быть и она будет два выходных подряд 11-12 и 18-19 июля 2020 на природе (18+)
date: 2020-07-02 00:00:00
categories: announcement
short_description: Чтоооо? Сразу две встречи на двух выходных подряд и на природе? да как такое возможно?!
image_preview: https://avatars1.githubusercontent.com/u/27823111?v=3&s=30
---

Привет,

Мы тут думали, выбирали что да сделать так, как собраться всем было удобно. Ну и, вообщем, решили что не надо выбирать когда выезжать на природу, а надо брать и собираться два раза. Да, вот такоей поворот :)

Итого получается что встреча 0x08 у нас будет состоять из двух частей (vol1, vol2) с палатками, костром, алкоголем, мясом и пловом, весельем, песнями под гитару и многим другим

- `0x08.vol1` 11-12 июля 2020 с заездом в пятницу(10)
- `0x08.vol2` 18-19 июля 2020 с заездом в пятницу(17)

Ориентировочные места проведения встреч (укажем оба, чтобы одно было свободное точно) [https://goo.gl/maps/SY8mgZJKDL2PboU96 (тут мы были в рошлом году)](https://goo.gl/maps/SY8mgZJKDL2PboU96) и второе [https://goo.gl/maps/jsztq9ErkJgMNsZv6 (новое)](https://goo.gl/maps/jsztq9ErkJgMNsZv6)

Форма записи на встречу [https://docs.google.com/forms/d/e/1FAIpQLSfPr65yVWlNTr95YFQPx2eWXUoUly6DLHkXeItiS2BsaIKxPA/viewform](https://docs.google.com/forms/d/e/1FAIpQLSfPr65yVWlNTr95YFQPx2eWXUoUly6DLHkXeItiS2BsaIKxPA/viewform?usp=sf_link)

---

P.S. Конечно, ко встречам нужно подготовить маски и санитайзеры, а если есть желающие быть спикерами — [велкам](https://dc20e6.ru/meetup/add-report) ;)
