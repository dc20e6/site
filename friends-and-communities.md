---
layout: default
excerpt: На этой странице список сообществ DC-ru и друзей нашего сообщества
permalink: friends-and-communities/
---

# Список сообществ и друзей DC78422

На этой странице мы собрали список сообществ DC-ru, друзей нашего сообщества и соседей :)

## Сообщества DEFCON-ru (DC-ru)

- [DC7812 - Санкт Петербург](https://defcon-russia.ru/)
- [DC7499 - Москва](http://defcon.su/)
- [DC78422 (DC20e6, DC8422) - Ульяновск](https://dc20e6.ru/)
- [DC7831 - Нижний Новгород](http://defcon-nn.ru/)
- [DC7495 - Зеленоград](https://dc7495.org/)
- [DC7343 - Екатеринбург](https://t.me/defcon7343)
- [DC7342 - Пермь](https://t.me/dc7342_news)
- [DC78412 - Пенза](http://defcon58.ru/)
- [DC7863 - Ростов-на-Дону](https://t.me/dc7863)
- [DC78182 - Архангельск](https://t.me/dc78182)
- [DC7423 - Владивосток](https://t.me/dc7423)
- [DC8800 – У них нет города, они просто делают свое дело](http://dc8800.ru/)
- Полный список DC сообществ по всему миру доступен по ссылке: [https://defcongroups.org/dcpages-int.html](https://defcongroups.org/dcpages-int.html)

---

## Соседи и друзья DC78422

### [Party Hack - самая киберпанковская конференция по информационной безопасности](https://partyhack.ru/)

> Не признаются что делают, но делают хорошую конференцию :)

### [Flipper Addons](https://flipperaddons.com/)

> Укрась и усиль свой Flipper так, как ты хочешь и расширь функциональность через модули [СС1101](https://flipperaddons.com/product/subghz-compact/), NRF24L01, да даже iButton удобнее сделай или выбери [что еще](https://flipperaddons.com/shop/)

### [ULCAMP — пляжный IT-фестиваль](https://ulcamp.ru/)

> ULCAMP — пляжный IT-фестиваль, куда съезжаются специалисты со всей России и даже из-за границы. Почти каждое лето лагерь ULCAMP разворачивается в живописном месте на берегу Волги, и с пятницы по воскресенье сотни людей ведут там интеллектуальные разговоры, попивают коктейли в пляжных барах и пританцовывают на вечерних концертах.

### [Pentest Awards: Первая в России премия для пентестеров](https://award.awillix.ru/)

> Мы сделали Pentest Awards, чтобы этичные хакеры наконец смогли заявить о себе, рассказать о своих достижениях и получить признание отрасли.

### [СТАЧКА](https://nastachku.ru/)

> "Yet another" IT-конференция.

### [Сообщество Самарских работников и новичков сферы ИТ.](https://sitc.community)

> Задачей группы является коллаборация между специалистами, работодателями, организациями и новичками в сфере ИТ.
>
> Все это служит одной общей цели - повышение уровня развития отрасли информационных технологий в нашем регионе.
> Сильное сообщество помогает подготавливать молодых специалистов, развиваться людям с опытом и налаживать сеть социальных контактов между представителями самарского ИТ.

---

<section>

  <h2>При поддержке</h2>

  <div class="grid t-hackcss-cards">

    {% for sponsor in site.sponsor %}

    <div class="cell -6of12 t-hackcss-cards-cell">
      <div class="card">
        <header class="card-header"><a href="{{ sponsor.link }}" title="{{ sponsor.name }}" target="_blank">{{ sponsor.name }}</a></header>
        <div class="card-content">
          <div class="inner">

            {% if sponsor.image %}
            <img src="{{ sponsor.image }}" class="t-hackcss-cards-image"
                 alt="{{ sponsor.name }}"
                 title="{{ sponsor.name }}" />
            {% endif %}

            <p class="t-hackcss-cards-text">{{ sponsor.description }}</p>
          </div>
        </div>
      </div>
    </div>

    {% endfor %}

  </div>
</section>

